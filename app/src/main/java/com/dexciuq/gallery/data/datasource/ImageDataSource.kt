package com.dexciuq.gallery.data.datasource

import com.dexciuq.gallery.domain.model.Image
import io.reactivex.rxjava3.core.Single

interface ImageDataSource {
    fun getImages(): Single<List<Image>>
}