package com.dexciuq.gallery.data.datasource

import com.dexciuq.gallery.data.mapper.ImageMapper
import com.dexciuq.gallery.data.network.ApiService
import com.dexciuq.gallery.domain.model.Image
import io.reactivex.rxjava3.core.Single

class PicsumPhotosDataSource(
    private val apiService: ApiService,
    private val mapper: ImageMapper,
    private val randomChooser: RandomChooser,
) : ImageDataSource {
    override fun getImages(): Single<List<Image>> = apiService.getImages(
        randomChooser.choose(), 30
    ).map {
        it.map(mapper::toImage)
    }
}