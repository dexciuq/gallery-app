package com.dexciuq.gallery.data.network

import com.dexciuq.gallery.data.model.ImageDto
import io.reactivex.rxjava3.core.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("v2/list")
    fun getImages(
        @Query("page") page: Int,
        @Query("limit") limit: Int = 30,
    ): Single<List<ImageDto>>
}