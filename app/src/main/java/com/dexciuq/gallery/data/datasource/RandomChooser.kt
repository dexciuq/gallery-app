package com.dexciuq.gallery.data.datasource

import javax.inject.Inject

class RandomChooser @Inject constructor() {

    fun choose() = (1..30).random()
}