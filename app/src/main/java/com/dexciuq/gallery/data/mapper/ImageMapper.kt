package com.dexciuq.gallery.data.mapper

import com.dexciuq.gallery.data.model.ImageDto
import com.dexciuq.gallery.domain.model.Image
import javax.inject.Inject

class ImageMapper @Inject constructor() {
    fun toImage(dto: ImageDto) = Image(
        id = dto.id.toInt(),
        author = dto.author,
        width = dto.width,
        height = dto.height,
        url = dto.downloadUrl
    )
}