package com.dexciuq.gallery.domain.repository

import com.dexciuq.gallery.data.datasource.ImageDataSource
import com.dexciuq.gallery.domain.model.Image
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class ImageRepositoryImpl (
    private val dataSource: ImageDataSource
) : ImageRepository {
    override fun getImages(): Single<List<Image>> = dataSource.getImages()
}