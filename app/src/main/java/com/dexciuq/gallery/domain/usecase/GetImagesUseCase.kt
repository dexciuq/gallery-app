package com.dexciuq.gallery.domain.usecase

import com.dexciuq.gallery.domain.model.Image
import com.dexciuq.gallery.domain.repository.ImageRepository
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class GetImagesUseCase @Inject constructor (
    private val imageRepository: ImageRepository
) {
    operator fun invoke(): Single<List<Image>> = imageRepository.getImages()
}