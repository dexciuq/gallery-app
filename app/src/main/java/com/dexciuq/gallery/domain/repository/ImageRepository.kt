package com.dexciuq.gallery.domain.repository

import com.dexciuq.gallery.domain.model.Image
import io.reactivex.rxjava3.core.Single

interface ImageRepository {
    fun getImages(): Single<List<Image>>
}