package com.dexciuq.gallery.di

import com.dexciuq.gallery.presentation.MainActivity
import dagger.Component

@Component(modules = [AppModule::class])
interface AppComponent {
    fun inject(activity: MainActivity)
}