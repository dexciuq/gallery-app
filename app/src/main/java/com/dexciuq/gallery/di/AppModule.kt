package com.dexciuq.gallery.di

import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.viewmodel.initializer
import androidx.lifecycle.viewmodel.viewModelFactory
import com.dexciuq.gallery.data.datasource.ImageDataSource
import com.dexciuq.gallery.data.datasource.PicsumPhotosDataSource
import com.dexciuq.gallery.data.datasource.RandomChooser
import com.dexciuq.gallery.data.mapper.ImageMapper
import com.dexciuq.gallery.data.network.ApiService
import com.dexciuq.gallery.data.network.Retrofit
import com.dexciuq.gallery.domain.repository.ImageRepository
import com.dexciuq.gallery.domain.repository.ImageRepositoryImpl
import com.dexciuq.gallery.domain.usecase.GetImagesUseCase
import com.dexciuq.gallery.presentation.loader.GlideImageLoader
import com.dexciuq.gallery.presentation.loader.ImageLoader
import com.dexciuq.gallery.presentation.loader.PicassoImageLoader
import com.dexciuq.gallery.presentation.viewmodel.MainViewModel
import dagger.Module
import dagger.Provides
import javax.inject.Named

const val GLIDE = "glide"
const val PICASSO = "picasso"

@Module
class AppModule {

    @Provides
    @Named(GLIDE)
    fun provideGlideImageLoader(glideImageLoader: GlideImageLoader): ImageLoader {
        return glideImageLoader
    }

    @Provides
    @Named(PICASSO)
    fun providePicassoImageLoader(picassoImageLoader: PicassoImageLoader): ImageLoader {
        return picassoImageLoader
    }

    @Provides
    fun provideApiService(): ApiService = Retrofit.apiService

    @Provides
    fun provideImageDataSource(
        apiService: ApiService,
        imageMapper: ImageMapper,
        randomChooser: RandomChooser,
    ): ImageDataSource {
        return PicsumPhotosDataSource(apiService, imageMapper, randomChooser)
    }

    @Provides
    fun provideImageRepository(dataSource: ImageDataSource): ImageRepository {
        return ImageRepositoryImpl(dataSource)
    }

    @Provides
    fun provideMainViewModelFactory(getImagesUseCase: GetImagesUseCase): ViewModelProvider.Factory {
        return viewModelFactory {
            initializer {
                MainViewModel(getImagesUseCase)
            }
        }
    }
}