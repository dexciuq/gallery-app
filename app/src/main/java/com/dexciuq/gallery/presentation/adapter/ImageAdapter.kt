package com.dexciuq.gallery.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.dexciuq.gallery.databinding.ListItemImageBinding
import com.dexciuq.gallery.domain.model.Image
import com.dexciuq.gallery.presentation.loader.ImageLoader

class ImageAdapter(
    private val imageLoader: ImageLoader,
    private val onClickListener: (String) -> Unit,
) : ListAdapter<Image, ImageAdapter.ViewHolder>(ImageDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            ListItemImageBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(getItem(position))

    inner class ViewHolder(
        private val binding: ListItemImageBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(image: Image) {
            imageLoader.load(image.url, binding.image)
            binding.root.setOnClickListener{ onClickListener(image.url) }
        }
    }
}