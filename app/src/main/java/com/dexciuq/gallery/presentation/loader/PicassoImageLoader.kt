package com.dexciuq.gallery.presentation.loader

import android.widget.ImageView
import com.squareup.picasso.Picasso
import javax.inject.Inject

class PicassoImageLoader @Inject constructor() : ImageLoader {
    override fun load(url: String, imageView: ImageView) {
        Picasso.get()
            .load(url)
            .into(imageView)
    }
}