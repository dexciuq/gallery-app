package com.dexciuq.gallery.presentation.adapter

import androidx.recyclerview.widget.DiffUtil
import com.dexciuq.gallery.domain.model.Image

class ImageDiffCallback : DiffUtil.ItemCallback<Image>() {

    override fun areItemsTheSame(oldItem: Image, newItem: Image): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Image, newItem: Image): Boolean {
        return oldItem == newItem
    }
}