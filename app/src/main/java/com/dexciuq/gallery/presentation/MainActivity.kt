package com.dexciuq.gallery.presentation

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.ViewModelProvider
import com.dexciuq.gallery.databinding.ActivityMainBinding
import com.dexciuq.gallery.databinding.ListItemImageBinding
import com.dexciuq.gallery.di.DaggerAppComponent
import com.dexciuq.gallery.di.GLIDE
import com.dexciuq.gallery.presentation.adapter.ImageAdapter
import com.dexciuq.gallery.presentation.loader.ImageLoader
import com.dexciuq.gallery.presentation.viewmodel.MainViewModel
import javax.inject.Inject
import javax.inject.Named

class MainActivity : AppCompatActivity() {

    @Inject lateinit var viewModelFactory: ViewModelProvider.Factory
    @Inject @Named(GLIDE) lateinit var imageLoader: ImageLoader
    private val binding by lazy { ActivityMainBinding.inflate(layoutInflater) }
    private val adapter by lazy { ImageAdapter(imageLoader, ::itemOnClickListener) }
    private val viewModel by viewModels<MainViewModel> { viewModelFactory }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        DaggerAppComponent.create().inject(this)
        setContentView(binding.root)
        binding.recyclerView.adapter = adapter
        viewModel.fetchRandomImages()
        setObservers()
        setListeners()
    }

    private fun setListeners() {
        binding.swipeRefreshLayout.setOnRefreshListener {
            binding.swipeRefreshLayout.isRefreshing = true
            viewModel.fetchRandomImages()
        }
    }

    private fun setObservers() {
        viewModel.images.observe(this) {
            binding.swipeRefreshLayout.isRefreshing = false
            adapter.submitList(it)
        }

        viewModel.error.observe(this) {
            if (it) {
                binding.swipeRefreshLayout.isRefreshing = false
                Toast.makeText(
                    this@MainActivity,
                    "Cannot load images",
                    Toast.LENGTH_SHORT
                ).show()
                viewModel.clearError()
            }
        }
    }

    private fun itemOnClickListener(url: String) {
        val view = ListItemImageBinding.inflate(layoutInflater)
        val dialogBuilder = AlertDialog.Builder(this)
            .setView(view.root)
            .setPositiveButton("Close") { it, _ -> it.dismiss() }

        dialogBuilder.create().show()
        imageLoader.load(url, view.image)
    }
}