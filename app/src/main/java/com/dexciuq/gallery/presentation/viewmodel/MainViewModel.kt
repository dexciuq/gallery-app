package com.dexciuq.gallery.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.dexciuq.gallery.data.datasource.PicsumPhotosDataSource
import com.dexciuq.gallery.data.mapper.ImageMapper
import com.dexciuq.gallery.data.network.Retrofit
import com.dexciuq.gallery.domain.model.Image
import com.dexciuq.gallery.domain.repository.ImageRepositoryImpl
import com.dexciuq.gallery.domain.usecase.GetImagesUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.core.Single
import io.reactivex.rxjava3.disposables.CompositeDisposable
import io.reactivex.rxjava3.schedulers.Schedulers
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

class MainViewModel(
    private val getImagesUseCase: GetImagesUseCase
) : ViewModel() {

    private val disposable = CompositeDisposable()

    private val _images = MutableLiveData<List<Image>>()
    val images: LiveData<List<Image>> = _images

    private val _error = MutableLiveData<Boolean>()
    val error: LiveData<Boolean> = _error

    fun fetchRandomImages() {
        getImagesUseCase()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                _images.value = it
            }, {
                _error.value = true
            })
            .run(disposable::add)
    }

    fun clearError() { _error.value = false }
}