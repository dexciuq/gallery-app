package com.dexciuq.gallery.presentation.loader

import android.widget.ImageView

interface ImageLoader {
    fun load(url: String, imageView: ImageView)
}